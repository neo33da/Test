package com.komarev.test.activities.show;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.ImageViewState;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.komarev.test.Constants;
import com.komarev.test.R;
import com.komarev.test.activities.BaseActivity;
import com.komarev.test.models.Photo;
import com.komarev.test.utils.IntentUtils;
import com.komarev.test.utils.StringUtils;
import com.komarev.test.utils.rx.RxUtils;
import com.komarev.test.web.Api;
import com.komarev.test.web.images.DecoderFactory;
import com.komarev.test.web.images.PicassoDecoderFactory;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.Picasso;
import com.trello.rxlifecycle.ActivityEvent;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShowActivity extends BaseActivity
        implements SubsamplingScaleImageView.OnImageEventListener {

    private static final String TAG = "ShowActivity";
    private static final String EXTRA_PHOTO = "extra_photo";
    private static final String BUNDLE_PHOTO = "bundle_state";
    private static final String BUNDLE_STATE = "bundle_state";
    private static final String BUNDLE_LOADED = "bundle_loaded";

    @Inject
    OkHttpClient mHttpClient;
    @Inject
    Api mApi;

    @Bind(R.id.image)
    SubsamplingScaleImageView mImage;
    @Bind(R.id.info)
    TextView mInfo;
    @Bind(R.id.progress)
    ProgressBar mProgress;

    private Picasso mPicasso;
    private Photo mPhoto;
    private boolean mLoaded;

    public static void start(Context context, Photo photo) {
        Intent intent = new Intent(context, ShowActivity.class);
        intent.putExtra(EXTRA_PHOTO, photo);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);
        getComponent().inject(this);
        ButterKnife.bind(this);
        ImageViewState imageViewState = null;
        if (savedInstanceState == null) {
            mPhoto = getIntent().getExtras().getParcelable(EXTRA_PHOTO);
        } else {
            mPhoto = savedInstanceState.getParcelable(BUNDLE_PHOTO);
            mLoaded = savedInstanceState.getBoolean(BUNDLE_LOADED);
            imageViewState = !savedInstanceState.containsKey(BUNDLE_STATE)
                    ? null
                    : (ImageViewState) savedInstanceState.getSerializable(BUNDLE_STATE);
        }
        if (mPhoto == null) {
            throw new IllegalArgumentException("Missing photo " + TAG);
        }
        updateInfo();
        loadImage(imageViewState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        ImageViewState state = mImage.getState();
        if (state != null) {
            outState.putSerializable(BUNDLE_STATE, state);
        }
        outState.putParcelable(BUNDLE_PHOTO, mPhoto);
        outState.putBoolean(BUNDLE_LOADED, mLoaded);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void updateInfo() {
        StringBuilder builder = new StringBuilder(getString(R.string.text_photo));
        mInfo.setText(builder
                .append(mPhoto.getName())
                .append(StringUtils.NEW_LINE)
                .append(getString(R.string.text_author))
                .append(mPhoto.getUser() == null
                        ? StringUtils.EMPTY
                        : mPhoto.getUser().getFullName())
                .append(StringUtils.NEW_LINE)
                .append(getString(R.string.text_camera))
                .append(mPhoto.getCamera() == null ? StringUtils.EMPTY : mPhoto.getCamera()));
    }

    private void loadImage(@Nullable ImageViewState state) {
        if (mLoaded) {
            initImage(state);
            return;
        }
        mApi.getPhoto(mPhoto.getId(), Constants.FEATURE, Constants.CONSUMER_KEY)
                .compose(RxUtils.applySchedulersAndRetry())
                .compose(bindUntilEvent(ActivityEvent.DESTROY))
                .subscribe(response -> {
                    mLoaded = true;
                    mPhoto = response.getPhoto();
                    updateInfo();
                    initImage(state);
                }, throwable -> {
                    throwable.printStackTrace();
                    showToast(R.string.text_loading_error);
                });
    }

    private void initImage(@Nullable ImageViewState state) {
        mPicasso = Picasso.with(mImage.getContext());
        mImage.setBitmapDecoderFactory(new PicassoDecoderFactory(mPhoto.getImageUrl(), mPicasso));
        mImage.setRegionDecoderFactory(new DecoderFactory(mHttpClient));
        mImage.setImage(ImageSource.uri(mPhoto.getImageUrl()), state);
        mImage.setOnImageEventListener(this);
    }

    @OnClick(R.id.fab)
    void onFabClicked() {
        IntentUtils.shareUrl(this, mPhoto.getImageUrl(),
                getString(R.string.text_check_out_this_pic));
    }

    @Override
    protected void onDestroy() {
        if (!TextUtils.isEmpty(mPhoto.getImageUrl())) {
            mPicasso.cancelTag(mPhoto.getImageUrl());
            mPicasso = null;
            mImage.recycle();
            mImage = null;
        }
        super.onDestroy();
    }

    @Override
    public void onReady() {
        mProgress.setVisibility(View.GONE);
    }

    @Override
    public void onImageLoaded() {
        mProgress.setVisibility(View.GONE);
    }

    @Override
    public void onPreviewLoadError(Exception e) {
        mProgress.setVisibility(View.GONE);
        e.printStackTrace();
    }

    @Override
    public void onImageLoadError(Exception e) {
        mProgress.setVisibility(View.GONE);
        showToast(R.string.text_image_loading_error);
    }

    @Override
    public void onTileLoadError(Exception e) {
        mProgress.setVisibility(View.GONE);
        e.printStackTrace();
    }
}
