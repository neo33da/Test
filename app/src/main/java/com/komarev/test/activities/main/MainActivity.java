package com.komarev.test.activities.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.komarev.test.Constants;
import com.komarev.test.R;
import com.komarev.test.activities.BaseActivity;
import com.komarev.test.activities.show.ShowActivity;
import com.komarev.test.adapters.PhotosAdapter;
import com.komarev.test.models.Photo;
import com.komarev.test.storage.preferences.Prefs;
import com.komarev.test.utils.EndlessRecyclerOnScrollListener;
import com.komarev.test.utils.rx.RxUtils;
import com.komarev.test.web.Api;
import com.trello.rxlifecycle.ActivityEvent;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements PhotosAdapter.OnItemClickListener {

    private static final String TAG = "MainActivity";

    @Inject
    Prefs mPrefs;
    @Inject
    Api mApi;
    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private PhotosAdapter mAdapter;
    private EndlessRecyclerOnScrollListener mEndlessListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getComponent().inject(this);
        ButterKnife.bind(this);
        initViews();
        loadPage(1, true);
    }

    private void initViews() {
        LinearLayoutManager layoutManager = new GridLayoutManager(this, 2);
        mAdapter = new PhotosAdapter(this, this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setHasFixedSize(true);
        mEndlessListener = new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                loadPage(current_page, false);
            }
        };
        mRecyclerView.addOnScrollListener(mEndlessListener);
    }

    private void loadPage(int page, boolean replace) {
        mApi.getPhotoList(Constants.FEATURE, Constants.CONSUMER_KEY, page)
                .compose(RxUtils.applySchedulersAndRetry())
                .compose(bindUntilEvent(ActivityEvent.DESTROY))
                .subscribe(list -> {
                    mAdapter.setItemList(list.getPhotos(), replace);
                }, throwable -> {
                    throwable.printStackTrace();
                    showToast(R.string.text_loading_error);
                });
    }

    @Override
    public void onItemClicked(@NonNull Photo item, int position) {
        ShowActivity.start(this, item);
    }
}
