package com.komarev.test;


public class Constants {

    public static final String PACKAGE_NAME = "com.testproject.komarev";
    public static final String BASE_URL = "https://api.500px.com/v1/";
    public static final String FEATURE = "popular";
    public static final String CONSUMER_KEY = "wB4ozJxTijCwNuggJvPGtBGCRqaZVcF6jsrzUadF";
    public static final int TIMEOUT_DURATION_SEC = 120;
}
