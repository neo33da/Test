package com.komarev.test.injects;


import com.komarev.test.App;
import com.komarev.test.activities.main.MainActivity;
import com.komarev.test.activities.show.ShowActivity;
import com.komarev.test.adapters.PhotosAdapter;

import dagger.Component;


@PerApp
@Component(modules = {
        ApplicationModule.class,
        NetworkModule.class,
        GlobalModule.class
})
public interface ApplicationComponent {

    void inject(PhotosAdapter adapter);

    void inject(ShowActivity activity);

    void inject(MainActivity baseActivity);

    void inject(App app);
}
