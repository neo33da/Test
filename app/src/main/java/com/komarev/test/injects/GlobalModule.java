package com.komarev.test.injects;

import com.komarev.test.App;
import com.komarev.test.storage.preferences.Prefs;
import com.komarev.test.storage.preferences.PrefsImpl;
import com.komarev.test.web.images.ImageLoader;
import com.komarev.test.web.images.PicassoImageLoader;

import dagger.Module;
import dagger.Provides;

@Module
public class GlobalModule {

    private App mApp;

    public GlobalModule(App app) {
        mApp = app;
    }

    @Provides
    @PerApp
    ImageLoader provideImageLoader() {
        return new PicassoImageLoader(mApp);
    }

    @Provides
    @PerApp
    Prefs providePrefs() {
        return new PrefsImpl(mApp);
    }

}
