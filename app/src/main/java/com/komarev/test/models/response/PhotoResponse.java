package com.komarev.test.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.komarev.test.models.Photo;

/**
 * Created by Ромка on 01.06.2016.
 */
public class PhotoResponse {

    @SerializedName("photo")
    @Expose
    private Photo mPhoto;

    public Photo getPhoto() {
        return mPhoto;
    }

    public void setPhoto(Photo photo) {
        mPhoto = photo;
    }
}
