package com.komarev.test.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ромка on 01.06.2016.
 */
public class Photo implements Parcelable {

    @SerializedName("id")
    @Expose
    private long mId;

    @SerializedName("name")
    @Expose
    private String mName;

    @SerializedName("image_url")
    @Expose
    private String mImageUrl;

    @SerializedName("camera")
    @Expose
    private String mCamera;

    @SerializedName("user")
    @Expose
    private User mUser;

    public Photo() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeString(mName);
        dest.writeString(mImageUrl);
        dest.writeString(mCamera);
        dest.writeParcelable(mUser, flags);
    }

    protected Photo(Parcel in) {
        mId = in.readLong();
        mName = in.readString();
        mImageUrl = in.readString();
        mCamera = in.readString();
        mUser = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String url) {
        mImageUrl = url;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

    public String getCamera() {
        return mCamera;
    }

    public void setCamera(String camera) {
        mCamera = camera;
    }
}
