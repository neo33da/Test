package com.komarev.test.models.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.komarev.test.models.Photo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ромка on 01.06.2016.
 */
public class PhotoListResponse {

    @SerializedName("current_page")
    @Expose
    private int mCurrentPage;

    @SerializedName("total_pages")
    @Expose
    private int mTotalPages;

    @SerializedName("photos")
    @Expose
    private List<Photo> mPhotos = new ArrayList<>();

    public int getCurrentPage() {
        return mCurrentPage;
    }

    public void setCurrentPage(int currentPage) {
        mCurrentPage = currentPage;
    }

    public int getTotalPages() {
        return mTotalPages;
    }

    public void setTotalPages(int totalPages) {
        mTotalPages = totalPages;
    }

    public List<Photo> getPhotos() {
        return mPhotos;
    }

    public void setPhotos(List<Photo> photos) {
        mPhotos = photos;
    }
}
