package com.komarev.test;


import android.app.Application;

import com.komarev.test.injects.ApplicationComponent;
import com.komarev.test.injects.ApplicationModule;
import com.komarev.test.injects.DaggerApplicationComponent;
import com.komarev.test.injects.GlobalModule;
import com.komarev.test.injects.NetworkModule;
import com.komarev.test.web.images.ImageLoader;

import javax.inject.Inject;

public class App extends Application {

    @Inject
    ImageLoader imageLoader;

    private ApplicationComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .networkModule(new NetworkModule(this))
                .globalModule(new GlobalModule(this))
                .build();
        mComponent.inject(this);
    }

    public ApplicationComponent getComponent() {
        return mComponent;
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        imageLoader.dropMemoryCache();
    }
}
