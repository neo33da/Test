package com.komarev.test.web.images;

import com.davemorrissey.labs.subscaleview.decoder.ImageDecoder;
import com.squareup.picasso.Picasso;

/**
 * Created by Ромка on 09.04.2016.
 */
public class PicassoDecoderFactory
        implements com.davemorrissey.labs.subscaleview.decoder.DecoderFactory<ImageDecoder> {

    private final String mTag;
    private final Picasso mPicasso;

    public PicassoDecoderFactory(String tag, Picasso picasso) {
        this.mTag = tag;
        this.mPicasso = picasso;
    }

    public ImageDecoder make() {
        return new PicassoDecoder(mTag, mPicasso);
    }
}