package com.komarev.test.web;


import com.komarev.test.models.response.PhotoListResponse;
import com.komarev.test.models.response.PhotoResponse;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface Api {

    @GET("photos")
    Observable<PhotoListResponse> getPhotoList(
            @Query("feature") String feature,
            @Query("consumer_key") String consumerKey,
            @Query("page") Integer page
    );

    @GET("photos/{id}")
    Observable<PhotoResponse> getPhoto(
            @Path("id") long id,
            @Query("feature") String feature,
            @Query("consumer_key") String consumerKey
    );

}
