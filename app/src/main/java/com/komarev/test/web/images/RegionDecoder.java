package com.komarev.test.web.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;

import com.davemorrissey.labs.subscaleview.decoder.ImageRegionDecoder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;

import java.io.InputStream;

/**
 * Created by Ромка on 09.04.2016.
 */
public class RegionDecoder implements ImageRegionDecoder {

    private static final String TAG = "RegionDecoder";

    private OkHttpClient mClient;
    private BitmapRegionDecoder mDecoder;
    private final Object mDecoderLock = new Object();

    public RegionDecoder(OkHttpClient client) {
        this.mClient = client;
    }

    @Override
    public Point init(Context context, Uri uri) throws Exception {
        OkHttpDownloader downloader = new OkHttpDownloader(mClient);
        InputStream inputStream = downloader.load(uri, 0).getInputStream();
        this.mDecoder = BitmapRegionDecoder.newInstance(inputStream, false);
        return new Point(this.mDecoder.getWidth(), this.mDecoder.getHeight());
    }

    @Override
    public Bitmap decodeRegion(Rect rect, int sampleSize) {
        synchronized (this.mDecoderLock) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = sampleSize;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            Bitmap bitmap = this.mDecoder.decodeRegion(rect, options);
            if (bitmap == null) {
                throw new RuntimeException(
                        "Region decoder returned null bitmap - image format may not be supported");
            } else {
                return bitmap;
            }
        }
    }

    @Override
    public boolean isReady() {
        return this.mDecoder != null && !this.mDecoder.isRecycled();
    }

    @Override
    public void recycle() {
        this.mDecoder.recycle();
    }
}