package com.komarev.test.web.images;

import com.davemorrissey.labs.subscaleview.decoder.ImageRegionDecoder;
import com.squareup.okhttp.OkHttpClient;

/**
 * Created by Ромка on 09.04.2016.
 */
public class DecoderFactory
        implements com.davemorrissey.labs.subscaleview.decoder.DecoderFactory<ImageRegionDecoder> {

    private OkHttpClient mHttpClient;

    public DecoderFactory(OkHttpClient httpClient) {
        mHttpClient = httpClient;
    }

    public ImageRegionDecoder make() {
        return new RegionDecoder(mHttpClient);
    }
}