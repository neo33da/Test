package com.komarev.test.web.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;

import com.davemorrissey.labs.subscaleview.decoder.ImageDecoder;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

/**
 * Created by rkom on 11.04.16.
 */
public class PicassoDecoder implements ImageDecoder {
    private String mTag;
    private Picasso mPicasso;

    public PicassoDecoder(String tag, Picasso picasso) {
        this.mTag = tag;
        this.mPicasso = picasso;
    }

    @Override
    public Bitmap decode(Context context, Uri uri) throws Exception {
        return mPicasso
                .load(uri)
                .tag(mTag)
                .config(Bitmap.Config.RGB_565)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .get();
    }
}