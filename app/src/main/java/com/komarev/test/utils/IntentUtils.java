package com.komarev.test.utils;

import android.content.Context;
import android.content.Intent;

import com.komarev.test.R;

/**
 * Created by Ромка on 01.06.2016.
 */
public class IntentUtils {

    private final static String FORMAT_TEXT_PLAIN = "text/plain";

    public static void shareUrl(Context context, String url, String subject) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType(FORMAT_TEXT_PLAIN);
        intent.putExtra(Intent.EXTRA_TEXT, url);
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.text_share)));
    }

}
