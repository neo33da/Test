package com.komarev.test.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.komarev.test.App;
import com.komarev.test.R;
import com.komarev.test.adapters.viewholder.PhotoItemViewHolder;
import com.komarev.test.models.Photo;
import com.komarev.test.web.images.ImageLoader;

import javax.inject.Inject;

/**
 * Created by Ромка on 01.06.2016.
 */
public class PhotosAdapter extends BaseAdapter<PhotoItemViewHolder, Photo>
        implements View.OnClickListener {

    private static final String TAG = "PhotosAdapter";

    @Inject
    ImageLoader mImageLoader;

    private OnItemClickListener mListener;

    public interface OnItemClickListener {

        void onItemClicked(@NonNull Photo item, int position);
    }

    public PhotosAdapter(@NonNull Context context, @Nullable OnItemClickListener listener) {
        super(context);
        ((App) context.getApplicationContext()).getComponent().inject(this);
        mListener = listener;
    }

    @Override
    public PhotoItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PhotoItemViewHolder(getInflater().inflate(R.layout.item_photo, parent, false));

    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            int position = (int) v.getTag();
            switch (v.getId()) {
                case R.id.layout:
                    mListener.onItemClicked(getItem(position), position);
                    break;
            }
        }
    }

    @Override
    public void onBindViewHolder(PhotoItemViewHolder holder, int position) {
        Photo photo = getItem(position);
        holder.setTag(position);
        holder.setOnClickListener(this);
        holder.setTitle(photo.getName());
        mImageLoader.displayImage(photo.getImageUrl(), holder.getImage());
    }
}
