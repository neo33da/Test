package com.komarev.test.adapters.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.komarev.test.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by rkom on 01.06.16.
 */
public class PhotoItemViewHolder extends BaseViewHolder {

    @Bind(R.id.layout)
    View mLayout;
    @Bind(R.id.image)
    ImageView mImage;
    @Bind(R.id.title)
    TextView mTitle;
    @Bind(R.id.progressBar)
    View mProgress;

    public PhotoItemViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    @Override
    public void setTag(int position) {
        mLayout.setTag(position);
    }

    @Override
    public void setOnClickListener(View.OnClickListener listener) {
        mLayout.setOnClickListener(listener);
    }

    public ImageView getImage() {
        return mImage;
    }

    public void setTitle(String title) {
        mTitle.setText(title);
    }
}
