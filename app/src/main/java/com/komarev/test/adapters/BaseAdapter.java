package com.komarev.test.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;

import com.komarev.test.adapters.viewholder.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rkom on 01.06.16.
 */
public abstract class BaseAdapter<VH extends BaseViewHolder, I> extends RecyclerView.Adapter<VH> {

    private List<I> mItems;
    private LayoutInflater mInflater;
    protected boolean mLoading;

    public BaseAdapter(@NonNull Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    protected LayoutInflater getInflater() {
        return mInflater;
    }

    public void setItemList(@Nullable List<I> items, boolean replaceList) {
        if (items == null) {
            return;
        }
        if (replaceList) {
            mItems = items;
        } else {
            addItemListNotNotify(items);
        }
        notifyDataSetChanged();
    }

    private void addItemListNotNotify(@NonNull List<I> items) {
        if (mItems == null) {
            mItems = new ArrayList<>();
        }
        mItems.addAll(items);
    }

    public void setItemNotNotify(int position, @NonNull I item) {
        mItems.set(position, item);
    }

    public void setLoadingState(boolean newState) {
        boolean previousLoadingState = mLoading;
        mLoading = newState;
        if (!newState && previousLoadingState) {
            notifyDataSetChanged();
        }
    }

    public void clear() {
        if (mItems != null) {
            mItems.clear();
            notifyDataSetChanged();
        }
    }

    public void removeItem(int position) {
        if (position >= getItemCount()) {
            return;
        }
        mItems.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mItems.size()); // fixing positioning tags
    }

    public boolean getLoadingState() {
        return mLoading;
    }

    public boolean isLoading() {
        return mLoading;
    }

    @Nullable
    public List<I> getItemList() {
        return mItems;
    }

    public I getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public int getItemCount() {
        return (mItems == null) ? 0 : mItems.size();
    }
}