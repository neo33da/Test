package com.komarev.test.adapters.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by rkom on 01.06.16.
 */
public abstract class BaseViewHolder extends RecyclerView.ViewHolder {

    public BaseViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void setTag(int position);

    public abstract void setOnClickListener(View.OnClickListener listener);

}